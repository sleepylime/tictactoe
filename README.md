## OthelloReversi

C++ project using the SFML Framework to make a game of Othello/Reversi. 
Deal with the project name. You asked me to make it.

Play Othello [here](http://www.othelloonline.org/)!

---

## Getting SFML

1. **Download SFML** [here](https://www.sfml-dev.org/download.php).
2. Select the **stable** version.
3. Choose **Visual C++ 14 (2015)** since we are using Visual Studio 2017.
4. Extract the .zip file to any directory you wish. (I put it in my C:\ drive).

---

## Making Project and Integrating SFML

1. Create a Win32 Application Project (Choose whether it's console or window application).
2. Made a main C++ file. Just using Main.cpp.
3. Go to C/C++ settings in the Project Settings.
4. Follow remaining steps [here](https://www.sfml-dev.org/tutorials/2.4/start-vc.php) as well as header file settings etc.

---

** Do note that you don't need to make the project, since I am doing/did it already for the Master branch. Do download SFML of course.**

---

## Branch Structure

There will be two main branches for the project. The Master branch, where all the final changes we have go, and the Develop branch, where we will add changes.
When we end for the day, we will finalize the changes for the Develop branch, and I will update the Master branch with the Develop branch. The Master branch will
be our "backup" for the next day of working on things.

Hopefully we don't need to go into making feature branches off the Develop branch. But if we do need to resort to that, I will let you know how that works.

In your case, **when you clone this repository, select the develop branch to clone.** I will deal with updating the Master branch.